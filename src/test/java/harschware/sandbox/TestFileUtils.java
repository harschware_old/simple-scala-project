package harschware.sandbox;


import static org.junit.Assert.assertTrue;

import java.net.URL;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TestFileUtils {
	private static final Logger LOGGER = LoggerFactory
			.getLogger(TestFileUtils.class);

	private static final ClassLoader LOADER = Thread.currentThread()
			.getContextClassLoader();
	URL testFileUrl = LOADER.getResource("FileUtils-test.txt");

	@Test
	public void testFileRead() {
		String testFileContent = FileUtils.readFile(testFileUrl.getPath());
		LOGGER.info("++" + testFileContent + "++");
		assertTrue(testFileContent.equals("test"));
	} // end test
} // end test class
