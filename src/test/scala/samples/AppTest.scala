package samples

import java.net.URL
import org.junit._
import org.junit.Assert._
import harschware.sandbox.FileUtils
import org.slf4j.LoggerFactory

@Test
class AppTest {
  var LOGGER = LoggerFactory
    .getLogger(classOf[AppTest]);

  val fileName = "FileUtils-test.txt"
  val testFileUrl = Thread.currentThread()
    .getContextClassLoader().getResource(fileName)

  @Test
  def testResourceLoading() = {
    assertTrue(getClass.getClassLoader().getResourceAsStream(fileName) != null)
    assertTrue(getClass.getClassLoader().getResourceAsStream("/" + fileName) == null)
    assertTrue(getClass.getResourceAsStream(fileName) == null)
    assertTrue(getClass.getResourceAsStream("/" + fileName) != null)
    assertTrue(classOf[AppTest].getResourceAsStream(fileName) == null)
    assertTrue(classOf[AppTest].getResourceAsStream("/" + fileName) != null)
  }

  @Test
  def testJava() = {
    val testFileContent = FileUtils.readFile(testFileUrl.getPath());
    LOGGER.info(testFileContent)

    assertTrue(testFileContent.equals("test"));
  }

  @Test
  def testScala() = {
    val testFileContent = io.Source.fromInputStream(getClass.getResourceAsStream("/" + fileName)).mkString
    LOGGER.info(testFileContent)
    assertTrue(testFileContent.equals("test"));
  }
}


