package harschware.sandbox;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;

public class FileUtils {
	private FileUtils() {} // prevents instantiation
	
	/**
	 * See http://stackoverflow.com/questions/326390/how-to-create-a-java-string-from-the-contents-of-a-file
	 * 
	 * @param path File path
	 * @return File contents
	 */
	public static String readFile(String path) {
		try {
			FileInputStream stream = new FileInputStream(new File(path));
			try {
				FileChannel fc = stream.getChannel();
				MappedByteBuffer bb = fc.map(FileChannel.MapMode.READ_ONLY, 0,
						fc.size());
				/* Instead of using default, pass in a decoder. */
				return Charset.defaultCharset().decode(bb).toString();
			} catch (IOException ioe) {
				throw new RuntimeException(ioe);
			} finally {
				stream.close();
			} // end try/catch/finally
		} catch (IOException ioe) {
			throw new RuntimeException(ioe);
		} // end try/catch		
	} // end static method

}
